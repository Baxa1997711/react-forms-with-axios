import axios from "axios";
import React, { useState } from "react";
import FormInput from "../FormInput/FormInput";
import './form.css'
import MButton from "../MButton/MButton";

const Form = () => {
  const [loading, setLoading] = useState(false);
  const initialValues = {
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
  };
  const [values, setValues] = useState(initialValues);

  const inputs = [
    {
      name: "username",
      type: "text",
      errorMessage: "Username is required, must be at least 3 characters",
      label: "Username",
      value: values.username,
      required: true,
      pattern: "^[a-zA-Z0-9]{3,}$",
    //   onChange: (e) => setValues({ ...values, username: e.target.value })
    },
    {
      name: "email",
      type: "email",
      errorMessage: "Email is required",
      label: "Email",
      value: values.email,
      required: true,
      pattern:
        "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$",
    //   onChange: (e) => setValues({ ...values, email: e.target.value })
    },
    {
      name: "password",
      type: "password",
      errorMessage: "Password is required, must be at least 6 characters",
      label: "Password",
      value: values.password,
      required: true,
      pattern: "^[a-zA-Z0-9]{6,13}$",
    //   onChange: (e) => setValues({ ...values, password: e.target.value })
    },
    {
      name: "confirmPassword",
      type: "password",
      errorMessage: "Confirm Password should match with Password",
      label: "Confirm Password",
      value: values.confirmPassword,
      required: true,
      pattern: values.password,
    //   onChange: (e) => setValues({ ...values, confirmPassword: e.target.value })
    },
  ];

  const onChange = (e) => {
    console.log('e.target.name', e.target.name);
    console.log('e.target.value', e.target.value);
    setValues({ ...values, [e.target.name]: e.target.value })
  }

  const onSubmit = (e) => {
    e.preventDefault();
    // const data = new FormData(e.target);
    const formData = {
      // username: data.get("username"),
      // email: data.get("email"),
      // password: data.get("password"),
      // confirmPassword: data.get("confirmPassword"),
      ...values,
    };
    console.log("formData", formData);
    setLoading(true);
    axios
      .post("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/user", formData)
      .then((res) => {
        setLoading(false);
        setValues(initialValues);
        // getUsers();
      });
  };

  return (
    <>
      <div className="formWrapper">
        <h2>Create User</h2>
        <form className="form" onSubmit={onSubmit}>
          {inputs.map((input) => (
            <FormInput
              key={input.name}
                {...input}
                onChange={onChange}
            />
          ))}
          {/* <FormInput
            placeholder="Username"
            label="Username"
            type="text"
            name="username"
            value={values.username}
            {...inputs[0]}
            // handleChange={(e) => setValues({ ...values, username: e.target.value })}
            // onChange={inputs[0].onChange}
          />
          <FormInput
            placeholder="Email"
            label="Email"
            type="email"
            name="email"
            value={values.email}
            {...inputs[1]}
            // handleChange={(e) => setValues({ ...values, email: e.target.value })}
            // onChange={inputs[1].onChange}
          />
          <FormInput
            placeholder="Password"
            label="Password"
            type="password"
            name="password"
            value={values.password}
            {...inputs[2]}
            // handleChange={(e) => setValues({ ...values, password: e.target.value })}
            // onChange={inputs[2].onChange}
          />
          <FormInput
            placeholder="Confirm Password"
            label="Confirm Password"
            type="password"
            name="confirmPassword"
            value={values.confirmPassword}
            {...inputs[3]}
            // handleChange={(e) => setValues({ ...values, confirmPassword: e.target.value })}
            // onChange={inputs[3].onChange}
          /> */}
          <MButton
            BLoading={loading}
            BSize={"submit"}
            BStyle={{ marginBottom: "15px" }}
          >
            Submit
          </MButton>
        </form>
      </div>
    </>
  );
};

export default Form;
