import React from "react";
import { ReactComponent as ActionIcon } from "../../Assets/Images/action.svg";
import { ReactComponent as DeleteIcon } from "../../Assets/Images/delete.svg";
import MButton from "../MButton/MButton";
import "./userList.css";

const UserList = ({ users, deleteUser }) => {
  return (
    <div className="userList">
      <h1>Users</h1>
      <div className="static default">
        <div className="item">Username</div>
        <div className="item">Email</div>
        <div className="item">Password</div>
        <ActionIcon className="item action action__btn" />
      </div>
      {users &&
        users.map((user) => (
          <div className="default" key={user?.id}>
            <div className="item">{user?.username}</div>
            <div className="item">{user?.email}</div>
            <div className="item">{user?.password}</div>
            <MButton
              onClick={() => {
                console.log('delete user',user?.id);
                deleteUser(user?.id)
              }}
              BClass="item action action__delete"
              BType={"outline-primary"}
              BIcon={<DeleteIcon />}
            />
          </div>
        ))}
    </div>
  );
};

export default UserList;
